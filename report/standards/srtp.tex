\subsection[SRTP]{Secure Real-time Transport Protocol\index{SRTP}}
\label{subsection:srtp}
\paragraph{} Das \emph{Secure Real-time Transport Protocol} (SRTP) ist ein Profil f\"ur RTP, welches Vertraulichkeit, Authentifikation und Schutz gegen Replay-Attacken\index{Replay-Attacke}~\footnote{Wiederspiegelung von bereits versandten Nachrichten} bietet. Diese Dienste werden sowohl f\"ur den RTP-Datenstrom wie auch f\"ur RTCP geboten. SRTP spezifiziert zu diesem Zweck kryptographische Transformationen und bietet Mechanismen, wie neue Transformationen in SRTP integriert werden k\"onnen.\\
SRTP baut auf dem ``RTP/AVP''-Profil~\cite{RTP-AVP} auf. So gilt dieses Profil f\"ur alle Bereiche, die von SRTP nicht explizit spezifiziert werden.
\paragraph{} Konzeptionell ist das Protokol ein \emph{``Bump in the Stack''~\footnote{Einschub im Protokollstapel}}\index{Bump in the Stack} zwischen der RTP-Applikation und dem Transport-Layer: als Eingabe verarbeitet es senderseitig RTP-Pakete, die es als SRTP-Pakete an den darunterliegenden Layer weiterleitet. Auf Empf\"angerseite erfolgt dieser Vorgang in umgekehrter Reihenfolge.
\paragraph{} SRTP k\"ummert sich nicht ums Schl\"ussel\-management. Dies muss von einem entsprechenden Protokoll wie GDOI~\ref{subsection:gdoi} oder GSAKMP~\ref{subsection:gsakmp} \"uber\-nom\-men werden.
\paragraph{} Secure RTCP bietet RTCP\index{RTCP} dieselben Sicherheitsleistungen wie SRTP dies f\"ur RTP tut. Ein Unterschied ist, dass bei SRTCP Authentifizierung zwingend ist.
\paragraph{} SRTP hat folgende Eigenschaften:
\begin{itemize}
    \item Vertraulichkeit f\"ur RTP und RTCP.
    \item Integrit\"at und Replay-Schutz f\"ur RTP- und RTCP-Pakete.
    \item Durch neue kryptographische Transformationen erweiterbar.
    \item Tiefe Bandbreitenbelastung durch Erhalt der RTP-Headerkompression.
    \item Geringer zus\"atzlicher Rechen- und Speicheraufwand.
    \item Erhalt der Unabh\"angigkeit des darunterliegenden Transportprotokolls und Mediums.
    \item Erleichterung des Schl\"usselmanagements durch Einsatz eines Master-Schl\"ussels zum Ableiten von weiterem Schl\"usselmaterial.
\end{itemize}
\paragraph{} Durch die geringf\"ugige Erweiterung\index{Erweiterung} der RTP- und RTCP-Pakete bleiben die positiven RTP-Eigenschaften, wie hohe Durchsatzrate und Einsetzbarkeit im heterogenen Umfeld erhalten. So eignet sich SRTP auch f\"ur den Einsatz bei drahtlosen Ger\"aten und Applikationen.

\subsubsection{SRTP\index{SRTP}}
\paragraph{} SRTP authentifiziert das gesamte RTP-Paket\index{RTP-Paket} und verschl\"usselt einzig das Data-Payload. Dessen Gr\"osse der Daten bleibt durch die kryptographische Transformation identisch, da kein Padding\index{Padding}~\footnote{Auff\"ullen von Bits} vorgenommen wird. 
\begin{figure}[!htb]\begin{center}
    \includegraphics[width=0.8\textwidth]{images/srtp-packet-structure}
    \caption{SRTP Paketstruktur}
    \label{img:srtp-packet-structure}
\end{center}\end{figure}
\paragraph{} Die Struktur eines SRTP-Pakets\index{SRTP-Paket} veranschaulicht Abbildung~\ref{img:srtp-packet-structure}, in der die optionalen Felder blau eingef\"arbt sind. Man erkennt, dass das RTP-Paket um zwei Felder erweitert wurde. Der optionale \emph{Master Key Index}\index{MKI} (\useGlosentry{glos:MKI}{MKI}) und das empfohlene \emph{Authentication Tag}\index{Authentication Tag} werden an das RTP-Paket angeh\"angt.
\paragraph{} Der MKI wird durch das Schl\"usselmanagement vorgegeben und identifiziert den Master Schl\"ussel, von dem das weitere Schl\"usselmaterial abgeleitet wird. Dazu geh\"ort beispielsweise der Sessionschl\"ussel.
\paragraph{} Das Authentication Tag enth\"alt die Daten zur Authentifikation des RTP-Paketes. Das Tag authentifiziert auch die Sequenznummer\index{Sequenznummer}, wodurch Schutz gegen Replay-Attacken\index{Replay-Attacke} geboten wird.
\subsubsection{SRTCP\index{SRTCP}}
\paragraph{} Analog werden dem SRTCP-Paket die beiden Felder (MKI, Authentication Tag) plus ein SRTCP Index angef\"ugt. Der Index enth\"alt die Sequenznummer\index{Sequenznummer} des SRTCP-Pakets. Im Gegensatz zu SRTP, wo der entsprechende Index via \useGlosentry{glos:ROC}{ROC}\index{ROC} und \useGlosentry{glos:SEQ}{SEQ} bestimmt werden muss, wird dieser in jedem Paket explizit mitgef\"uhrt. Ausf\"uhrungen zur Bestimmung des SRTP-Index\index{SRTP-Index} finden sich im n\"achsten Abschnitt oder im Kapitel 3.3.1 der RFC 3711~\cite{SRTP}.
\paragraph{} In SRTCP ist Authentifikation obligatorisch, weshalb das Authentication Tag nicht optional ist. Dies garantiert die Integrit\"at der SRTCP-Pakete, denn b\"osartig ver\"anderte RTCP-Nachrichten k\"onnten ansonsten den RTP-Stream unterbrechen.
\paragraph{} Die Verwendung des MKI ist wie bei SRTP optional.
\subsubsection{Kryptographischer Kontext\index{Krypto-Kontext}}
\label{subsubsection:crypto-context}
\paragraph{} Jeder SRTP-Stream zwischen Sender und Empf\"anger ben\"otigt kryptographische Statusinformationen\index{Statusinformation}. Diese bilden in SRTP den sogenannten kryptographischen Kontext. Bei Mehrteilnehmerkonferenzen m\"ussen diese Informationen durch das Schl\"usselmanagement bereitgestellt werden. Da dies bei der zu entwickelnden Applikation der Fall ist, werden die wichtigsten Parameter in diesem Abschnitt kurz vorgestellt.
\begin{description}
    \item[Rollover Counter - ROC\index{ROC}]{~\\
        Der Rollover Counter (32 bit) z\"ahlt, wie oft die RTP Sequenznummer (16 bit) wegen \"Uberlaufs auf 0 zur\"uckgesetzt wurde. Der Index eines SRTP-Pakets errechnet aus der Konkatenation von ROC und SEQ, was einen 48-bit Wert ergibt.
    }
    \item[Verschl\"usselung\index{Verschl\"usselung}]{~\\
        Dieser Parameter kennzeichnet den verwendeten Verschl\"usselungs\-al\-go\-rith\-mus und dessen Modus.
    }
    \item[Authentifikation\index{Authentifikation}]{~\\
        Dieser Parameter kennzeichnet den verwendeten Authentifikationsalgorithmus.
    }
    \item[MKI\index{MKI}]{~\\
        Der Identifikator des momentan verwendeten Master-Schl\"ussels. Dieser Parameter ist optional und wird nur ben\"otigt, wenn mehrere Master-Schl\"ussel zum Einsatz kommen.
    }
    \item[$<$From,To$>$\index{$<$From,To$>$}]{~\\
        Diese Werte legen die G\"ultigkeitsdauer eines Master-Schl\"ussels fest. Die beiden Werte sind SRTP-Packetindizes und definieren ein Intervall. Sie dienen der Identifikation des aktuellen Master-Schl\"ussels und sind eine Alternative zum MKI. Sie sind ebenfalls optional.
    }
    \item[Master Key(s)\index{Master Key}]{~\\
        Der eigentliche Master-Schl\"ussel. Es k\"onnen auch mehrere sein, die alle mit einem eindeutigen MKI verkn\"upft sind.
    }
    \item[Master Salt\index{Master Salt}]{~\\
        Ein kryptographisches Salz, welches zusammen mit dem Master Schl\"us\-sel verwendet wird.
    }
\end{description}
\paragraph{} Die Master Keys k\"onnen f\"ur SRTP und die entsprechen SRTCP-Session identisch sein, da die daraus generierten Sessionschl\"ussel unterschiedlich sein werden.
\paragraph{} SRTP definiert zwei Standard Verschl\"usselungs\-transformationen basierend auf der Blockchiffre \useGlosentry{glos:AES}{AES}~\cite{AES}\index{AES}: AES Counter Mode und AES f8-Mode. AES im f8-Mode wird als Verschl\"usselungsmechanismus in \useGlosentry{glos:UMTS}{UMTS}\index{UMTS} eingesetzt. Nebst diesen beiden k\"onnen aber neue Mechanismen zum Einsatz mit SRTP spezifiziert werden. Dies muss \"uber die Publikation eines \useGlosentry{glos:RFC}{Standards Track RFCs} geschehen, was zum Beispiel bei~\cite{BIG-AES} der Fall ist. Jedem neuen Algorithmus wird von der \useGlosentry{glos:IANA}{IANA~\footnote{Internet Assigned Numbers Authority}}\index{IANA} ein bestimmter Wert zugewiesen, der dann im Verschl\"usselungs\-parameter angegeben werden kann. Dies ist der Migrationspfad f\"ur zuk\"unftige Verschl\"usselungsalgorithmen.
\paragraph{} Dasselbe gilt auch f\"ur die Authentifikationsmechanismen. Standardm\"assig ist nur die Verwendung von \useGlosentry{glos:HMAC}{HMAC-SHA1}~\cite{HMAC}\index{MAC} vorgesehen. Die Unterst\"utzung von \useGlosentry{glos:TESLA}{TESLA} als Authentifikationsmechanismus wurde in~\cite{TESLA-SRTP} standardisiert, wodurch es von SRTP vollumf\"anglich unterst\"utzt wird.
\paragraph{} Je nach verwendetem Verschl\"usselungs- und Authentifikationsmechanismus muss das Schl\"usselmanagement\index{Schl\"usselmanagement} weitere, spezifische Parameter signalisieren.
\paragraph{} Ein kryptographischer Kontext\index{Krypto-Kontext} von SRTP wird durch das Triplet \texttt{SSRC, Ziel IP-Adresse, Ziel Portnummer} und nicht durch den MKI identifiziert. Der MKI bezeichnet lediglich den aktuell verwendeten Master Schl\"ussel. Durch das Triplet wird ein SRTP-Paket eindeutig einem Krypto-Kontext zugeordnet und kann dementsprechend verarbeitet werden. Kann einem Paket jedoch kein solcher Kontext zugewiesen werden, muss dieses Paket verworfen werden. Dadurch wird zum Beispiel ein \useGlosentry{glos:DoS}{DoS-Angriff}\index{DoS} erschwert.
