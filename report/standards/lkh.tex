\subsection[LKH]{Logical Key Hierarchy\index{LKH}}
\label{subsection:lkh}
\begin{figure}[!htb]\begin{center}
    \includegraphics[width=0.8\textwidth]{images/lkh-example}
    \caption{Beispiel eines LKH Baumes}
    \label{img:lkh-example}
\end{center}\end{figure}
\paragraph{} Logical Key Hierarchy (LKH)~\cite{LKH} erm\"oglicht ein effizientes Rekeying\index{Rekeying} f\"ur Gruppen mit grosser Teilnehmerzahl. Die Grundidee von LKH ist, dass die Schl\"ussel der Phase 2, die KEKs\index{KEK}~\footnote{Verschl\"usseln die Transfer Encryption Keys (\useGlosentry{glos:TEK}{TEKs}\index{TEK}) der Phase 3}, mit Hilfe einer Baumstruktur verwaltet werden. Alle Knoten enthalten kryptographische Schl\"ussel und die Hosts bilden die Bl\"atter des Baumes. Der eigentliche Gruppenschl\"ussel ist die Wurzel des Baumes. Die Knotenschl\"ussel sind als Untergruppen-Schl\"ussel anzusehen.
\paragraph{} Die Schl\"usselverteilvorschrift besagt, dass jedes Mitglied die Schl\"ussel auf dem Pfad von seinem Blatt bis zur Wurzel erh\"alt. Abbildung~\ref{img:lkh-example} zeigt schematisch die Schl\"usselhierarchie einer Gruppe mit 8 Hosts. Host A hat Kenntnis von den eingef\"arbten Knotenschl\"usseln.

\subsubsection{Initialisierung}
\paragraph{} Zur Initialisierung eines LKH-Baumes erstellt der \useGlosentry{glos:GCKS}{GCKS} als erstes einen Bin\"arbaum, wobei die Anzahl Bl\"atter derjenigen der Mitglieder entspricht. Dann speichert er den entsprechenden Phase 1 Schl\"ussel im jeweiligen Blattknoten. Weiter generiert er die Schl\"ussel der internen Knoten und verteilt diese verschl\"usselt durch die Schl\"ussel der beiden S\"ohne~\footnote{Folgeknoten}. Jeder interne Schl\"ussel wird deshalb mit zwei verschiedenen Schl\"usseln chiffriert und dann die Gruppe verschickt. Dadurch muss der GCKS $n-1$ Schl\"ussel an eine Gruppe der Gr\"osse $n$ senden.
\paragraph{} In einer anderen Strategie schickt er jedem Mitglied einzeln die ben\"otigten Knotenschl\"ussel. Diese Nachricht wird durch den jeweiligen Member Schl\"us\-sel chiffriert. Bei dieser Initialisierung muss der GCKS jedoch $O(nlogn)$ Nachrichten verschicken, was ineffizienter als die erste Variante ist.

\subsubsection{Hinzuf\"ugen eines Mitglieds}
\paragraph{} Wenn der Gruppe ein neuer Teilnehmer beitritt, muss dieser durch den GCKS entsprechend in den Baum eingef\"ugt werden. Die einfachste M\"og\-lich\-keit ist, wenn bereits ein leerer Platz im Baum existiert. Ist dies nicht der Fall, muss der GCKS durch Aufteilung eines Knotens neue Slots schaffen. Je nach (Baum-)Tiefe $l$ des aufgeteilten Knotens, werden entsprechend $2^{l}$ Pl\"atze geschaffen, wobei $l=0$ die Ebene der Bl\"atter ist.
\paragraph{} Nachdem dem neuen Blattknoten ein Schl\"ussel zugewiesen wurde, kann der GCKS die entsprechenden Knotenschl\"ussel mitteilen. Meist wird aber zuerst ein Rekeying durchgef\"uhrt und lediglich der Blattschl\"ussel bei der Registration mitgeteilt.

\subsubsection{Join Rekeying}
\paragraph{} Ein Rekeying beim Eintritt eines neuen Mitglieds ist n\"otig, um \emph{Backward Access Control}\index{Backward Access Control} zu garantieren. Ansonsten w\"are es einem solchen Teilnehmer m\"oglich, vergangene Daten zu entschl\"usseln, die er unter Umst\"anden relativ einfach aufzeichnen kann.
\paragraph{} Der GCKS generiert neue Schl\"ussel f\"ur alle Knoten auf dem Pfad vom neuen Mitglied zur Wurzel. Diese chiffriert er mit dem Blattschl\"ussel und sendet sie dann dem Teilnehmer mittels des Rekey Protokolls. Den bestehenden Teilnehmern schickt er die neuen Schl\"ussel, chiffriert mit den Schl\"usseln die sie ersetzen.
\paragraph{} Der Vorgang des Join Rekeyings wird in Abbildung~\ref{img:lkh-join-rekey} dargestellt. Host E ist das neue Mitglied und es wird angenommen, dass der GCKS den Blattknoten von Mitglied D splittet. Die neu generierten Schl\"ussel sind blau gef\"arbt. So auch der Schl\"ussel $K_e$, den der neue Teilnehmer E w\"ahrend der Registration erh\"alt. Die Chiffrierung und Schl\"usselverteilung geht wie folgt vonstatten:
\begin{itemize}
    \item $K_{a.e}$, $K_{c.e}$ und $K_{de}$ mit $K_{e}$ chiffriert an E.
    \item $K_{a.e}$ mit $K_{a.d}$ chiffriert an A, B, C und D.
    \item $K_{c.e}$ mit $K_{cd}$ chiffriert an C und D.
    \item $K_{de}$ mit $K_{d}$ chiffriert an D.
\end{itemize}
\begin{figure}[!htb]\begin{center}
    \includegraphics[width=0.8\textwidth]{images/lkh-join-rekey}
    \caption{Join Rekeying in LKH}
    \label{img:lkh-join-rekey}
\end{center}\end{figure}
\paragraph{} Generell muss der GCKS\index{GCKS} in einer Gruppe mit $n$ Mitgliedern $log_{2}n$ Schl\"ussel versenden. Wie dargelegt, chiffriert und \"ubermittelt er jeden Knotenschl\"ussel zweimal: einmal f\"ur den neuen Teilnehmer und einmal f\"ur die bestehenden Mitglieder. Im Endeffekt muss er also $2log_{2}n$ Schl\"ussel versenden.
\paragraph{} Die Effizienz kann durch LKH+\index{LKH+} gesteigert werden. Dabei werden die alten Schl\"ussel durch eine Einweg-Funktion transformiert, anstatt sie zu verwerfen. Somit reicht es, wenn der GCKS eine Signalisationsnachricht\index{Signalisierung} an die gesamte Gruppe schickt, denn jedes Mitglied kann die neuen Schl\"ussel selbst\"andig erzeugen. Der GCKS muss aber auch hier dem neuen Teilnehmer die Schl\"ussel senden.
\paragraph{} Im Vergleich zum ersten Ansatz wird mit LKH+ die Anzahl der zu chiffrierenden und \"ubermittelnden Schl\"ussel auf $log_{2}n$ halbiert.

\subsubsection{Leave Rekeying} 
\paragraph{} Beim Verlassen der Gruppe kann der GCKS im Prinzip einfach das Blatt des austretenden Mitglieds als frei markieren oder l\"oschen und somit den Baum verkleinern. Analog dem Join Rekeying gibt es aber hier auch sicherheitstechnische Bedenken, denn der ausgetretene Teilnehmer k\"onnte zu\-k\"unf\-ti\-ge \"Ubermittlungen (bis zum n\"achsten Rekeying) immer noch entschl\"usseln. Um dies zu verhindern und somit \emph{Forward Access Control}\index{Forward Access Control} zu gew\"ahrleisten muss beim Austreten des Mitglieds ein Rekeying durchgef\"uhrt werden. Die ihm bekannten Schl\"ussel sind auszutauschen.
\begin{figure}[!htb]\begin{center}
    \includegraphics[width=0.8\textwidth]{images/lkh-leave-rekey}
    \caption{Leave Rekeying in LKH}
    \label{img:lkh-leave-rekey}
\end{center}\end{figure}
\paragraph{} Der Vorgang des Leave Rekeyings wird in Abbildung~\ref{img:lkh-leave-rekey} dargestellt. Teilnehmer E wird aus der Gruppe entfernt. Das auszuschliessende Mitglied hat Kenntnis von $K_e$, $K_{de}$, $K_{c.e}$ und $K_{a.e}$, die im Diagramm blau eingef\"arbt sind. Diese m\"ussen ausgewechselt werden. Die Schl\"ussel $K_e$ und $K_{de}$ sind nicht mehr n\"otig und somit m\"ussen nur zwei Schl\"ussel (grau markiert) ausgetauscht werden. Die Chiffrierung und Schl\"usselverteilung geht wie folgt vonstatten:
\begin{itemize}
    \item $K_{a.d}$ mit $K_{ab}$ und $K_{cd}$ chiffriert an A, B, C und D.
    \item $K_{cd}$ mit $K_{c}$ und $K_{d}$ chiffriert C und D.
\end{itemize}
\paragraph{} Generell muss der GCKS\index{GCKS} alle Schl\"ussel im Besitz des austretenden Mitglieds austauschen, was der L\"ange des Pfades vom Mitglied zur Wurzel entspricht. In einem vollen Bin\"arbaum sind das $log_{2}n$ Knoten. Zudem wird jeder neue Schl\"ussel $K_{i}$ zweimal verschl\"usselt, n\"amlich einmal f\"ur jedes Kind des Knotens $i$. Im Endeffekt muss der GCKS also $2log_{2}n$ Schl\"ussel chiffrieren und versenden.
\paragraph{} Auch das Leave Rekeying kann durch Verwendung von Einwegfunktionen optimiert werden. Die sogenannten \emph{One-way Function Chains} (\useGlosentry{glos:OFC}{OFC}\index{One-way Function Chain}) basieren auf dem Prinzip, dass durch wiederholtes Anwenden einer Einwegfunktion\index{One-way Function} auf einen zuf\"alligen Wert $ri$ neue \useGlosentry{glos:KEK}{KEKs}\index{KEK} generiert werden. Kennt ein Mitlied das entsprechende $ri$ f\"ur seine Knoten, kann dieser durch mehrmaliges Anwenden der entsprechenden Einwegfunktion die Knotenschl\"ussel selbst generieren. Somit reicht es, wenn der GCKS eine Signalisationsnachricht pro Blatt- beziehungsweise Knotenschl\"ussel schickt, denn jedes Mitglied kann dann die neuen Schl\"ussel, bis hin zur Wurzel, selbst\"andig erzeugen.
\paragraph{} Im Vergleich zum \"ublichen Ansatz wird mit \useGlosentry{glos:OFC}{OFCs} die Anzahl der zu generierenden und \"ubermittelnden Schl\"ussel auf $log_{2}n$ halbiert.
\paragraph{} Dieser und weitere Algorithmen werden in~\cite{Mcast-Group-Security} Kapitel 6 detailliert besprochen. Das Referenzdokument der IETF ist~\cite{LKH}.
